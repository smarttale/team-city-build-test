﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptTest : MonoBehaviour
{
    public UnityEngine.UI.Text maText;

    private void Start()
    {
#if UNITY_PS4
        maText.text = "PS4";
#elif UNITY_STANDALONE
        maText.text = "Windows";
#elif UNITY_SWITCH
        maText.text = "Switch";
#else
        maText.text = "Other";
#endif

    }


}
