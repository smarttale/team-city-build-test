﻿namespace SmartTale.Framework.Core.Runtime.Utilities
{
    public interface IUpdatable
    {
        void Update(float a_deltaTime);
    }
}