﻿namespace SmartTale.Framework.Core.Runtime.Utilities.Extensions
{
    using System.Collections.Generic;

    public static class LinkedListExtensions
    {
        /// <summary>
        /// Remove and return the last value of the LinkedList
        /// </summary>
        /// <param name="a_list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>Last value</returns>
        public static LinkedListNode<T> Pop<T>(this LinkedList<T> a_list)
        {
            LinkedListNode<T> value = a_list.Last;
            a_list.RemoveLast();
            return value;
        }

        /// <summary>
        /// Remove and return the first value of the LinkedList
        /// </summary>
        /// <param name="a_list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>First value</returns>
        public static LinkedListNode<T> Shift<T>(this LinkedList<T> a_list)
        {
            LinkedListNode<T> value = a_list.First;
            a_list.RemoveFirst();
            return value;
        }
    }
}