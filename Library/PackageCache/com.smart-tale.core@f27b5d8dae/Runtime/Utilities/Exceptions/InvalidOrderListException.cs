﻿namespace SmartTale.Framework.Core.Runtime.Utilities.Exceptions
{
    using Runtime.Exceptions;

    public class InvalidOrderListException : SmartTaleException
    {
        public InvalidOrderListException() : base("Order list should have the same amount of items than the list") { }
        public InvalidOrderListException(string a_message) : base(a_message) { }
        public InvalidOrderListException(string a_message, SmartTaleException a_inner) : base(a_message, a_inner) { }   
    }
}