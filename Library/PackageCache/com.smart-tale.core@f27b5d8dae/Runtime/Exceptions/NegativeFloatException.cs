﻿namespace SmartTale.Framework.Core.Runtime.Exceptions
{
    public class NegativeFloatException : SmartTaleException
    {
        public NegativeFloatException() : base("A positive or null float was expected") { }
        public NegativeFloatException(string a_message) : base(a_message) { }
        public NegativeFloatException(string a_message, SmartTaleException a_inner) : base(a_message, a_inner) { }   
    }
}