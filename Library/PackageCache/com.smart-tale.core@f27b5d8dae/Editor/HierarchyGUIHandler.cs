﻿namespace SmartTale.Framework.Core.Editor
{
	using Runtime.Utilities;
	using UnityEditor;
	using UnityEngine;

	/// <summary>
	/// Handler for customise hierarchy GUI
	/// </summary>
	public static class HierarchyGUIHandler
	{
		private static Texture2D _placeholderLogoTexture = null;
		private static Texture2D PlaceholderLogoTexture
		{
			get
			{
				if (_placeholderLogoTexture == null)
					_placeholderLogoTexture = AssetDatabase.LoadAssetAtPath<Texture2D>("Packages/com.smart-tale.core/Gizmos/placeholder-logo.png");
				if (_placeholderLogoTexture == null)
					_placeholderLogoTexture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/SmartTale.Core/Gizmos/placeholder-logo.png");
				if (_placeholderLogoTexture != null)
					_placeholderLogoTexture.hideFlags = HideFlags.DontSaveInEditor;
				return _placeholderLogoTexture;
			}
		}

		private static string _placeholderTexturePath = string.Empty;
		
		[InitializeOnLoadMethod]
		private static void OnPackageLoadedInEditor()
		{
			if (PlaceholderLogoTexture == null) 
			{
				// After adding the pacakge to a project, we need to wait for one update cycle for the assets to load
				EditorApplication.update += OnPackageLoadedInEditor; 
			}
			else
			{
				EditorApplication.update -= OnPackageLoadedInEditor;
				EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyGUI; // Update hierarchy with logos
			}
		}


		private static void OnHierarchyGUI(int a_instanceID, Rect a_selectionRect)
		{
			GameObject instance = EditorUtility.InstanceIDToObject(a_instanceID) as GameObject;
			if (instance == null)
			{
				// Object in process of being deleted?
				return;
			}

			if (instance.GetComponent<Placeholder>() != null)
			{
				Rect texRect = new Rect(a_selectionRect.xMax - a_selectionRect.height, a_selectionRect.yMin, a_selectionRect.height, a_selectionRect.height);
				GUI.DrawTexture(texRect, PlaceholderLogoTexture, ScaleMode.ScaleAndCrop);
			}
		}
	}
}