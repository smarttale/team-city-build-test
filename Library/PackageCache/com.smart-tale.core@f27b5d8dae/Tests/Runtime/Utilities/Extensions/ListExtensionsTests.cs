﻿namespace SmartTale.Framework.Core.Tests.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using Runtime.Utilities.Exceptions;
    using NUnit.Framework;
    using SmartTale.Framework.Core.Runtime.Utilities.Extensions;

    public class ListExtensionsTests
    {
        [Test]
        public void ListRandomItemPasses()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                List<int> testList = new List<int>();
                testList.RandomItem();
            });
        }

        [Test]
        public void ListReOrderPasses()
        {
            // If there are less order arguments 
            Assert.Throws<InvalidOrderListException>(() =>
            {
                List<string> tmpList = new List<string>() { "aaa", "bbb", "ccc" };
                tmpList.ReOrder(new List<int>() { 0, 1 });
            });

            // If list is empty but not the order argument
            Assert.Throws<InvalidOrderListException>(() =>
            {
                List<string> tmpList = new List<string>();
                tmpList.ReOrder(new List<int>() { 0 });
            });
            
            // If there are more order arguments
            Assert.Throws<InvalidOrderListException>(() =>
            {
                List<string> tmpList = new List<string>() { "aaa", "bbb" };
                tmpList.ReOrder(new List<int>() { 0, 1, 3 });
            });
            
            // If the list is empty
            List<string> testList = new List<string>();
            testList.ReOrder(new List<int>() { });
            Assert.That(testList.Count, Is.EqualTo(0));
            
            // If the order doesn't change
            testList = new List<string>() { "aaa", "bbb", "ccc" };
            testList.ReOrder(new List<int>() { 0, 1, 2 });
            Assert.AreEqual(new List<string>() { "aaa", "bbb", "ccc" }, testList);
            
            // If the priority doesn't correspond to array indexes
            testList = new List<string>() { "aaa", "bbb", "ccc" };
            testList.ReOrder(new List<int>() { 0, 1, 3 });
            Assert.AreEqual(new List<string>() { "aaa", "bbb", "ccc" }, testList);
            
            // If the order change
            testList = new List<string>() { "aaa", "bbb", "ccc" };
            testList.ReOrder(new List<int>() { 1, 0, 2 });
            Assert.AreEqual(new List<string>() { "bbb", "aaa", "ccc" }, testList);

            // If some priority are the sames
            testList = new List<string>() { "aaa", "bbb", "ccc" };
            testList.ReOrder(new List<int>() { 5, 3, 5 });
            Assert.AreEqual(new List<string>() { "bbb", "aaa", "ccc" }, testList);
        }
    }
}