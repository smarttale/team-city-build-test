using UnityEngine;

namespace SmartTale.Framework.Attributes.Test
{
	public class RequireInterfaceTest : MonoBehaviour, IDummy
	{
		[RequireInterface(typeof(IDummy))]
		public Object requireDummyInterface;

		public RequireInterfaceNest1 nest1;
	}

	[System.Serializable]
	public class RequireInterfaceNest1
	{
		[RequireInterface(typeof(IDummy))]
		public Object requireDummyInterface;

		public RequireInterfaceNest2 nest2;
	}

	[System.Serializable]
	public struct RequireInterfaceNest2
	{
		[RequireInterface(typeof(IDummy))]
		public Object requireDummyInterface;
	}
}

public interface IDummy {}