﻿using System;

namespace SmartTale.Framework.Attributes
{
	public enum EConditionOperator
	{
		And,
		Or
	}
}
