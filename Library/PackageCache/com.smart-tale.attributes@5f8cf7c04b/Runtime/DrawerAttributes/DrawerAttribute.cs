﻿using UnityEngine;

namespace SmartTale.Framework.Attributes
{
	/// <summary>
	/// Base class for all drawer attributes
	/// </summary>
	public class DrawerAttribute : PropertyAttribute, INaughtyAttribute
	{
	}
}
