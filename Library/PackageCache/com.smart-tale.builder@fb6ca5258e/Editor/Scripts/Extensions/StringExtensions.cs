﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class StringExtensions
{
    private static string applicationDataPath = "";
    private static string _ApplicationDataPath
    {
        get
        {
            if (applicationDataPath == "") applicationDataPath = Application.dataPath;
            return applicationDataPath;
        }
    }


    public static string ConvertFullPathToProjectPath(this string a_fullPath)
    {
        if(a_fullPath.Length == 0)
        {
            Debug.LogWarning("Given path was empty!");
            return "";
        }
        string projectPath = _ApplicationDataPath.Replace("/Assets", "");
        if (a_fullPath.Contains(projectPath)) return a_fullPath.Replace(projectPath, "");
        //The file isn't included inside the project folder and therefore needs more relative pathing "../"
        //First find the folder from which they're separated
        
        else
        {
            Debug.Log($"Full path: {a_fullPath}, project path: {projectPath}");
            
            string[] argumentSubfolders = a_fullPath.Split('/');
            string[] projectSubfolders = projectPath.Split('/');
            int maxFolderCount = Mathf.Max(argumentSubfolders.Length, projectSubfolders.Length);
            string toReturn = "";
            
            for (int i = 0; i < maxFolderCount; i++)
            {
                //It means the previous folder was common but not this one
                if (argumentSubfolders[i] != projectSubfolders[i])
                {
                    //Assuming it's in the same drive
                    int amountOfDots = (projectSubfolders.Length - i);
                    for (int j = 0; j < amountOfDots; j++)
                    {
                        toReturn += "../";
                    }
                    for (int l = i; l < argumentSubfolders.Length; l++)
                    {
                        string addOn = (l == argumentSubfolders.Length - 1) ?"":"/";
                        toReturn += $"{argumentSubfolders[l]}{addOn}";
                    }
                    return toReturn;
                }
            }
            return toReturn;            
        }
    }

    /// <summary>
    /// Shortcuts the path to the one starting from the given subfolder.
    /// If C:/Toto/Lulu/Pepe/Image.png is given with a subfolder of Lulu, then Pepe/Image.png is returned
    /// Note that if the subfolder is not found, then an emtpy string is returned.
    /// </summary>
    /// <param name="a_currentPath"></param>
    /// <param name="a_subfolder"></param>
    /// <returns></returns>
    public static string GetPathAfterSubfolder(this string a_currentPath, string a_subfolder)
    {
        string[] subfolders = a_currentPath.Split('/');
        string localPath = "";
        for (int i = 0; i < subfolders.Length; i++)
        {
            if (subfolders[i] == a_subfolder)
            {
                localPath += "Assets/";
                for (int j = i + 1; j < subfolders.Length; j++)
                {
                    localPath += subfolders[j];
                    if (j != subfolders.Length - 1) localPath += "/";
                }
                return localPath;
            }
        }
        return localPath;
    }

    /// <summary>
    /// Converts a local path starting from the project folder or assets folder to a full path (starting from the drive).
    /// </summary>
    /// <param name="a_projectOrAssetsPath">The project relative path, be either directory or asset.</param>
    /// <returns></returns>
    public static string ConvertToFullPath(this string a_projectOrAssetsPath)
    {        
        string currentProjectPath = _ApplicationDataPath.Replace("Assets",""); 
        return currentProjectPath  + a_projectOrAssetsPath;
    }

    /// <summary>
    /// Removes the final slash of a directory path, if there is one, and returns it.
    /// </summary>
    /// <param name="a_path"></param>
    /// <returns></returns>
    public static string RemoveFinalFolderSlash(this string a_path)
    {
        if (a_path[a_path.Length - 1] == '/') return a_path.Substring(0, a_path.Length - 1);
        else return a_path;
    }

    /// <summary>
    /// Adds a slash at the end of a path if there is none, and returns it.
    /// </summary>    
    public static string AddSlash(this string a_path)
    {
        if (a_path.Length>0 && a_path[a_path.Length - 1] != '/')  a_path += "/";
        return a_path;
    }

    public static bool IsAbsolutePath(this string a_path)
    {
        if(a_path.Length == 0)
        {
            Debug.LogWarning("Path argument was emtpy");
            return false;
        }
        return a_path[1] == ':';
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="a_path"></param>
    /// <returns></returns>
    public static string GetDirectory(this string a_path)
    {
        string[] subfolders = a_path.Split('/');
        string localPath = "";
        for (int i = 0; i < subfolders.Length - 1; i++)
        {
            localPath += $"{subfolders[i]}/";
        }
        return localPath;
    }
}
