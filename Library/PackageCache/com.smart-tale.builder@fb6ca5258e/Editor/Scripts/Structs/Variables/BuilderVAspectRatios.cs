﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using Framework.Attributes;
    [System.Serializable]
    public class BuilderVAspectRatios : BaseBuilderVariable
    {
        [SerializeField][EnumFlags] CustomAspectRatios m_ratios;

        public CustomAspectRatios value
        {
            get => m_ratios;
            set { m_ratios = value; }
        }

        

        /// <summary>
        /// Does this preset have the given aspect ratio flagged or not?
        /// </summary>
        public bool HasAspectRatioFlagged(UnityEditor.AspectRatio a_aspect)
        {
            switch (a_aspect)
            {
                case UnityEditor.AspectRatio.AspectOthers:
                    return value.HasFlag(CustomAspectRatios.Others);
                case UnityEditor.AspectRatio.Aspect4by3:
                    return value.HasFlag(CustomAspectRatios.Aspect4by3);
                case UnityEditor.AspectRatio.Aspect5by4:
                    return value.HasFlag(CustomAspectRatios.Aspect5by4);
                case UnityEditor.AspectRatio.Aspect16by10:
                    return value.HasFlag(CustomAspectRatios.Aspect16by10);
                case UnityEditor.AspectRatio.Aspect16by9:
                    return value.HasFlag(CustomAspectRatios.Aspect16by9);
            }
            Debug.LogError("[BuildsAndPlayerPreset] No proper aspect ratio was checked!");
            return false;
        }


        /// <summary>
        /// Sets the given aspect ratio at the given value to this aspec ratio preset.
        /// </summary>        
        public void SetAspectRatioFlag(UnityEditor.AspectRatio a_aspect, bool a_valueToSet)
        {

            switch (a_aspect)
            {
                case UnityEditor.AspectRatio.AspectOthers:
                    if (a_valueToSet) value |= CustomAspectRatios.Others;
                    else value &= ~CustomAspectRatios.Others;
                    break;
                case UnityEditor.AspectRatio.Aspect4by3:
                    if (a_valueToSet) value |= CustomAspectRatios.Aspect4by3;
                    else value &= ~CustomAspectRatios.Aspect4by3;
                    break;
                case UnityEditor.AspectRatio.Aspect5by4:
                    if (a_valueToSet) value |= CustomAspectRatios.Aspect5by4;
                    else value &= ~CustomAspectRatios.Aspect5by4;
                    break;
                case UnityEditor.AspectRatio.Aspect16by10:
                    if (a_valueToSet) value |= CustomAspectRatios.Aspect16by10;
                    else value &= ~CustomAspectRatios.Aspect16by10;
                    break;
                case UnityEditor.AspectRatio.Aspect16by9:
                    if (a_valueToSet) value |= CustomAspectRatios.Aspect16by9;
                    else value &= ~CustomAspectRatios.Aspect16by9;
                    break;
                default:
                    Debug.LogError("[BuildsAndPlayerPreset] No proper aspect ratio was checked!");
                    break;
            }
        }
    }

}