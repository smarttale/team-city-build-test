﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    [System.Serializable]
    public class BuilderVScenes : BaseBuilderVariable
    {
        [SerializeField] public List<SceneAsset> m_scenes;

        public List<SceneAsset> value
        {
            get => m_scenes;
            set { m_scenes = value; }
        }

        public BuilderVScenes(bool a_enabled = true, string a_description = "my new variable")
        {
            this.enabled = a_enabled;
            m_description = a_description;
            
        }

        /// <summary>
        /// Gets the scenes of this settings in an EditorBuildSettingsScene array format.
        /// </summary>        
        public UnityEditor.EditorBuildSettingsScene[] GetPresetScenes()
        {
            List<EditorBuildSettingsScene> buildScenes = new List<EditorBuildSettingsScene>();
            for (int i = 0; i < value.Count; i++)
            {
                string path = AssetDatabase.GetAssetPath(value[i]);
                if (path == "") throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"Unable to get the scene path at index{i} on this build preset!");
                else buildScenes.Add(new EditorBuildSettingsScene(path, true));
            }
            return buildScenes.ToArray();

        }

        /// <summary>
        /// Gets the scenes of this preset and returns and orderly array of its local paths (e.g Assets/Scenes/SimpleScene.scene)
        /// This is the format that the build setting options need in the BuildPlayer script
        /// </summary>       
        public string[] GetPresetScenesPaths()
        {
            List<string> paths = new List<string>();
            for (int i = 0; i < value.Count; i++)
            {
                string path = AssetDatabase.GetAssetPath(value[i]);
                if (path == "") throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"Unable to get the scene path at index{i} on this build preset!");
                else paths.Add(path);
            }

            return paths.ToArray();
        }

        /// <summary>
        /// Sets the current scenes in the Build Settings to this preset.
        /// </summary>        
        public void SetCurrentToPresetScenes()
        {
            EditorBuildSettingsScene[] currentScenes = EditorBuildSettings.scenes;
            value = new List<SceneAsset>();
            foreach (EditorBuildSettingsScene scene in currentScenes)
            {
                if (scene.enabled)
                {
                    SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
                    if (sceneAsset == null) throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"Unable to get one of the scenes on the build settings!");
                    else value.Add(sceneAsset);
                }
            }
        }
    }

}