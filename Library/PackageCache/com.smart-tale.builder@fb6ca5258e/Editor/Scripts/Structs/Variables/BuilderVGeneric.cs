﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    [System.Serializable]
    public class BuilderVGeneric <T> : BaseBuilderVariable
    {
        [SerializeField] private T m_value;

        public T value
        {
            get => m_value;
            set { m_value = value; }
        }

        public BuilderVGeneric(T a_value, bool a_enabled = true, string a_description = "My new variable")
        {
            m_value = a_value;
            this.enabled = a_enabled;
            this.m_description = a_description;

        }

        public BuilderVGeneric(T a_value, bool a_enabled = true)
        {
            m_value = a_value;
            this.enabled = a_enabled;
        }

        public BuilderVGeneric(T a_value)
        {
            m_value = a_value;
        }

        public BuilderVGeneric() { }


        public static implicit operator T(BuilderVGeneric<T> lmao) => lmao.value;
    }

}