﻿namespace SmartTale.Framework.Builder
{ 
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Framework.Attributes;

    [System.Serializable]
    public abstract class BaseBuilderVariable 
    {
        [SerializeField] private bool m_enabled = true;
        protected string m_description = "my new variable";

        public bool enabled
        {
            get => m_enabled;
            set { m_enabled = value; }
        }


        
    }
}