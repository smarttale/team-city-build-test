﻿namespace SmartTale.Framework.Builder
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using System;
    using SmartTale.Framework.Attributes;

    /// <summary>
    /// The struct containing the actual preset of player and build settings.
    /// </summary>
    /// <remarks>
    /// The reason it is separated from the scriptable object is because it is easier to compare the presets in the JSON, and to separate the actual job of the scriptable object (buttons, asset, json, etc.) 
    /// and the struct (just storing the info). This way the struct can also handle itself the convertion between a serialized format and the format needed by the PlayerSettings or other (e.g. build settings scenes, or aspect ratios)
    /// It also needs to be public, or else Unity won't show it on the inspector.
    /// </remarks>
    [System.Serializable]
    public struct BuildAndPlayerSettings
    {
        [Header("Player Settings")]

        public BuilderVGeneric<string> m_companyName;
        public BuilderVGeneric<string> m_productName;
        public BuilderVGeneric<Texture2D> m_gameIcon;
        public BuilderVAspectRatios m_aspectRatios;
        [Header("Build Settings")]
        public BuilderVScenes m_scenes;
        public BuilderVGeneric<bool> m_developmentMode;
        public BuilderVGeneric<bool> m_autoConnectProfiler;
        public BuilderVGeneric<bool> m_scriptDebugging;
        public BuilderVGeneric<bool> m_waitForManagedDebugger;
        public BuilderVGeneric<bool> m_scriptsOnly;
        

        public void Initialize()

        {
            m_companyName = new BuilderVGeneric<string>("DefaultCompany");
            m_productName = new BuilderVGeneric<string>("Default Product", true, "The name og the game");
            m_gameIcon = new BuilderVGeneric<Texture2D>();
            m_aspectRatios = new BuilderVAspectRatios();            
            m_scenes = new BuilderVScenes();
            m_developmentMode = new BuilderVGeneric<bool>();
            m_autoConnectProfiler = new BuilderVGeneric<bool>();
            m_scriptDebugging= new BuilderVGeneric<bool>();
            m_waitForManagedDebugger= new BuilderVGeneric<bool>();
            m_scriptsOnly = new BuilderVGeneric<bool>();
        }
    } 
}
