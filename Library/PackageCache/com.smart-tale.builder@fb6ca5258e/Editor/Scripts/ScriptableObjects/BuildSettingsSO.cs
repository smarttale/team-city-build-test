﻿namespace SmartTale.Framework.Builder
{
    using UnityEngine;
    using UnityEditor;
    using SmartTale.Framework.Attributes;
    using System.IO;


    public partial class BuildSettingsSO : ScriptableObject, ISerializationCallbackReceiver
    {
        #region FIELDS
        #region Serialized

        public BuildTargetGroup m_buildTargetGroup = BuildTargetGroup.Standalone;
        public BuildTarget m_buildTarget = BuildTarget.StandaloneWindows;
        public bool m_buildAddressablesOrAssetBundles = false;
        public bool m_restorePlayerSettingsAfterBuild = true;
        public bool m_updatePreviousBuild;
        public BuildAndPlayerSettings m_settings;

        [HideInInspector]public bool m_canUpdatePreviousBuild;

        [HideInInspector] public string m_pathBuild = "";

        #endregion
        #region Internals
        private static bool m_useJsonAndSerialization = false;
        private bool m_runAfterBuild = false; 
        #endregion

        #region Properties
        private bool m_isCorrectTarget => EditorUserBuildSettings.activeBuildTarget == m_buildTarget;
        #endregion

        #endregion


        #region METHODS



        #region Lifecycle
        private void OnEnable()
        {
            if (m_fileSoRelativePath == "")
                UpdatePaths();
        } 
        #endregion


        #endregion

        #region Helper
        /// <summary>
        /// Gets the build preset with the path provided.
        /// If the preset doesn't exist, then a null value is returned.
        /// </summary>
        /// <param name="a_presetName">The name of the preset.</param>
        public static BuildSettingsSO SearchAndGetPreset(string a_presetName)
        {
            BuildSettingsSO settings = AssetDatabase.LoadAssetAtPath<BuildSettingsSO>(a_presetName);
            if (settings == null)
                throw new Core.Runtime.Exceptions.SmartTaleException($"Could not find a build preset at the given path!");            
            return settings;
        }
  

        [Button][HideIf(nameof(m_isCorrectTarget))]
        void SwitchTarget()
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(m_buildTargetGroup, m_buildTarget);
        }

        /// <summary>
        /// Builds using the current parameters of the preset.
        /// </summary>
        [Button] [ShowIf(nameof(m_isCorrectTarget))]
        void Build()
        {
            //Doesn't run after building
            //Selects a custom build folder, and saves the address
            m_runAfterBuild = false;

            TrySetBuildPath();
            BuildPlayer.BuildGeneral(this);
        }


        [Button][ShowIf(nameof(m_isCorrectTarget))]
        void BuildAndRun()
        {
            bool giveItATry = true;
            
            //Either we don't have a previous directory 
            //or the current target isn't the same as the last build
            //Get a new path
            if(m_pathBuild.Length == 0 
                || m_previousBuildTarget != m_buildTarget)
            {
                giveItATry = TrySetBuildPath();
            }
            //If the path was cancelled, then cancel the whole build
            if (giveItATry)
            {
                m_runAfterBuild = true;
                BuildPlayer.BuildGeneral(this);
            }
            
        }

        /// <summary>
        /// Saves the path where the build will be set
        /// </summary>
        /// <returns></returns>
        private bool TrySetBuildPath()
        {
            string defaultDirectory = "";
            if (m_pathBuild == "")
                defaultDirectory = Application.dataPath.Replace("Assets", "");
            else defaultDirectory = m_pathBuild;
            string locationPath = EditorUtility.SaveFolderPanel("Choose folder to save the build.", defaultDirectory, "");
            Debug.Log("Path selected: " + locationPath);
            if (locationPath.Length == 0)
            {
                return false;
            }
            else
            {
                m_pathBuild = locationPath;
                if (m_updatePreviousBuild)
                {
                    string fileName = m_settings.m_productName.enabled ? m_settings.m_productName.value : PlayerSettings.productName;
                    m_canUpdatePreviousBuild = Helper.DoesFileExistsNoExtension(locationPath, fileName);
                }
            }
            return true;
        }

        #endregion
    }


}
