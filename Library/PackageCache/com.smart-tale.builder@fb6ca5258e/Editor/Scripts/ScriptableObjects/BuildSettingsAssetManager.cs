﻿namespace SmartTale.Framework.Builder
{


    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using System.IO;
    using SmartTale.Framework.Attributes;
#if USE_ADDRESSABLES
    using UnityEditor.AddressableAssets;

    using UnityEditor.AddressableAssets.Settings;
#endif

    public partial class BuildSettingsSO //Asset manager
    {



        /// <summary>
        /// Relative path to the current preset file.
        /// </summary>
        private string m_fileSoRelativePath = "";
        /// <summary>
        /// Absolute path to the current json file.
        /// </summary>
        private string m_fileJsonAbsolutePath = "";
        /// <summary>
        /// Relative path to the current directory.
        /// </summary>
        private string m_fileDirectoryRelativePath = "";
        /// <summary>
        /// Absolute path to the current directory.
        /// </summary>
        private string m_fileDirectoryAbsolutePath = "";
        /// <summary>
        /// Backup name of the current preset, without extension.
        /// </summary>
        private string m_filePreviousName = "";

        /// <summary>
        /// The build target group saved before building (so we can switch platform before building and setting them back after building.
        /// </summary>
        private BuildTargetGroup m_previousBuildTargetGroup;
        /// <summary>
        /// The build target saved before building (so we can switch platform before building and setting them back after building.
        /// </summary>
        private BuildTarget m_previousBuildTarget;

        private BuildAndPlayerSettings m_playerSettingsBackup;

        /// <summary>
        /// Updates all the necessary paths, starting from this very scriptable objects. The other paths are calculated based on it.
        /// </summary>
        private void UpdatePaths()
        {
            m_fileSoRelativePath = AssetDatabase.GetAssetPath(this);
            if (m_fileSoRelativePath == "") return;
            m_fileJsonAbsolutePath = m_fileSoRelativePath.ConvertToFullPath().Replace(".asset", ".json");
            m_fileDirectoryRelativePath = m_fileSoRelativePath.Replace($"{name}.asset", "");
            m_fileDirectoryAbsolutePath = m_fileDirectoryRelativePath.ConvertToFullPath();
            m_filePreviousName = name;
        }

        /// <summary>
        /// Builds the assets bundle or the addressables accordingly to the settings.
        /// TODO: Have public parameters to configure what is built and how. For now there's just a boolean
        /// </summary>
        public void BuildAssetsBundle()
        {
#if USE_ADDRESSABLES
            if (m_buildAddressablesOrAssetBundles)
            {
                Debug.Log("[BuildPreset] Starting building Asset Bundles/Addressables!");
                //Assets bundles
                //BuildAssetBundleOptions options = BuildAssetBundleOptions.None;
                //options |= BuildAssetBundleOptions.ChunkBasedCompression;
                //options |= BuildAssetBundleOptions.ForceRebuildAssetBundle;
                //BuildPipeline.BuildAssetBundles("AssetsBundleFolder", options, BuildTarget.StandaloneWindows);
                //KilianHelper.CreateRecursiveDirectory("AssetsBundleFolder".ConvertToFullPath(), KilianHelper.PathType.Directory);

                //Directory
                EditorUserBuildSettings.SwitchActiveBuildTarget(m_buildTargetGroup, m_buildTarget);
                Debug.Log("Will build addressables!");
                AddressableAssetSettings.BuildPlayerContent();
                Debug.Log("[BuildPreset] Finished building Asset Bundles/Addressables!");

            }
#endif
        }






        /// <summary>
        /// Creates a new preset and sets it with the given group base settings
        /// </summary>
        /// <param name="group">The group type of the preset to look for (PS4/Standalone/XBOX)</param>
        /// <param name="fileName">The name of the preset.</param>
        /// <param name="original"></param>
        [MenuItem("Assets/Create/SmartTale/Framework/Build Preset")]
        private static void CreateNewPreset()
        {
            string relativeDirectory = AssetDatabase.GetAssetPath(Selection.activeObject) + "/";
            string name = "Preset Build";
            string extension = ".asset";
            string fullDirectory = relativeDirectory.ConvertToFullPath();
            string fullFilePathWithoutExtension = fullDirectory + name;
            string fullFilePath = fullDirectory + name + extension;

            Helper.CreateRecursiveDirectory(fullDirectory, Helper.PathType.Directory);


            /*
             * Then makes sure to not override an existing file placing an increasing number to the file name (get unique path method from AssetDatabase didn't seem to work)
             * TODO: After testing I realized it only adds a 0 in the end lol
             */
            int counter = 0;
            ///string fullFilePath = fullDirectory + "/" + fileName;

            bool foundUniqueFileName = false;

            if (File.Exists(fullFilePath))
            {
                Debug.Log("lmao exists");
                while (foundUniqueFileName == false)
                {
                    string newFilePath = fullFilePathWithoutExtension + counter + extension;
                    if (File.Exists(newFilePath))
                        counter++;

                    else
                    {
                        fullFilePath = newFilePath;
                        foundUniqueFileName = true;
                    }

                }
            }


            //Creates the preset, sets its value to current Player Settings values and creates its JSON file, then focus on it
            BuildSettingsSO newSettings = ScriptableObject.CreateInstance<BuildSettingsSO>();
            newSettings.isBeingCreated = true;
            EditorUtility.SetDirty(newSettings);
            newSettings.OverrideWithPlayerSettings();
            AssetDatabase.CreateAsset(newSettings, fullFilePath.ConvertFullPathToProjectPath());
            AssetDatabase.SaveAssets();
            Selection.activeObject = newSettings;
            EditorUtility.FocusProjectWindow();
            newSettings.UpdatePaths();
            if(m_useJsonAndSerialization) 
                newSettings.CreateJSONFile();
            newSettings.isBeingCreated = false;


            Debug.Log("[BuildPreset] Created new preset at location: \n" + fullFilePath);
        }


        #region APPLY OR GET SETTINGS



        /// <summary>
        /// Overrides the current preset settings with those already applied in the Player Settings or Build Settings.
        /// </summary>
        [Framework.Attributes.Button("Copy Current Player Settings")]
        public void OverrideWithPlayerSettings()
        {
            isCopyingFromPlayer = true;
            m_settings = OverrideCurrentSettings(m_settings);

            if(m_useJsonAndSerialization) SaveToJSON();
            isCopyingFromPlayer = false;
        }

        /// <summary>
        /// Applies the current preset settings to the player settings.
        /// </summary>
        [Framework.Attributes.Button("Apply to current player settings")]
        public void ApplyPlayerSettings()
        {
            m_playerSettingsBackup = new BuildAndPlayerSettings();
            m_playerSettingsBackup.Initialize();
            m_playerSettingsBackup = OverrideCurrentSettings(m_playerSettingsBackup);
            
            EditorUserBuildSettings.selectedStandaloneTarget = m_buildTarget;
            EditorUserBuildSettings.selectedBuildTargetGroup = m_buildTargetGroup;
            
            if (EditorUserBuildSettings.activeBuildTarget != m_buildTarget)
            {
                m_previousBuildTarget = m_buildTarget;
                m_previousBuildTargetGroup = m_buildTargetGroup;

                //if (!EditorUserBuildSettings.SwitchActiveBuildTarget(m_buildTargetGroup, m_buildTarget))
                //    Debug.LogError("Bad!!!!! Couldn't switch active build target!!!!");

            }
            ApplyPlayerSettings(m_settings);
        }

        /// <summary>
        /// Gets the current settings applied in the Player Settings and the Build Settings
        /// and overrides every value IF they are enabled.
        /// </summary>
        /// <returns></returns>

        internal static BuildAndPlayerSettings OverrideCurrentSettings(BuildAndPlayerSettings a_settings)

        {

            if(a_settings.m_productName.enabled) 
                a_settings.m_productName.value = PlayerSettings.productName;
            if (a_settings.m_companyName.enabled)
                a_settings.m_companyName.value = PlayerSettings.companyName;


            if (a_settings.m_gameIcon.enabled)
            {
                Texture2D[] icons = PlayerSettings.GetIconsForTargetGroup(BuildTargetGroup.Unknown);
                if (icons.Length > 0 && icons[0] != null) a_settings.m_gameIcon.value = icons[0]; 
            }
            if (a_settings.m_aspectRatios.enabled)
            {
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.AspectOthers, PlayerSettings.HasAspectRatio(AspectRatio.AspectOthers));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect5by4, PlayerSettings.HasAspectRatio(AspectRatio.Aspect5by4));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect4by3, PlayerSettings.HasAspectRatio(AspectRatio.Aspect4by3));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect16by9, PlayerSettings.HasAspectRatio(AspectRatio.Aspect16by9));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect16by10, PlayerSettings.HasAspectRatio(AspectRatio.Aspect16by10));
            }

            if(a_settings.m_developmentMode.enabled) a_settings.m_developmentMode.value = EditorUserBuildSettings.development;
            if(a_settings.m_scenes.enabled)
                a_settings.m_scenes.SetCurrentToPresetScenes();

            if (a_settings.m_scriptDebugging.enabled)
            {
                a_settings.m_scriptDebugging.value = EditorUserBuildSettings.allowDebugging;
               if(a_settings.m_waitForManagedDebugger.enabled) 
                    a_settings.m_waitForManagedDebugger.value = EditorUserBuildSettings.waitForManagedDebugger;
            }


            if (a_settings.m_autoConnectProfiler.enabled)
                a_settings.m_autoConnectProfiler.value = EditorUserBuildSettings.connectProfiler;


            if (a_settings.m_scriptsOnly.enabled) a_settings.m_scriptsOnly.value = EditorUserBuildSettings.buildScriptsOnly;
            return a_settings;
        }

        /// <summary>
        /// Generic static method to apply a given settings struct.
        /// </summary>
        /// <param name="a_settings">The struct with the settings to apply.</param>
        internal static void ApplyPlayerSettings(BuildAndPlayerSettings a_settings)
        {
            if (a_settings.m_productName.enabled) PlayerSettings.productName = a_settings.m_productName;
            if (a_settings.m_companyName.enabled) PlayerSettings.companyName = a_settings.m_companyName.value;

            //Icon needs an array of 1 tex2D, target unknown because it applies to every target group. 
            //Specifying which one sets it as if we were overriding the one already existing

            if (a_settings.m_gameIcon.value != null && a_settings.m_gameIcon.enabled)
            {
                Texture2D[] bruh = new Texture2D[] { a_settings.m_gameIcon };
                PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, bruh);
            }
            AssetDatabase.SaveAssets();

            if (a_settings.m_aspectRatios.enabled)
            {
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect16by10, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect16by10));
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect16by9, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect16by9));
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect4by3, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect4by3));
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect5by4, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect5by4));
                PlayerSettings.SetAspectRatio(AspectRatio.AspectOthers, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.AspectOthers)); 
            }

            if(a_settings.m_scenes.enabled) EditorBuildSettings.scenes = a_settings.m_scenes.GetPresetScenes();
            if (a_settings.m_developmentMode.enabled) EditorUserBuildSettings.development = a_settings.m_developmentMode;
            if (a_settings.m_autoConnectProfiler.enabled) EditorUserBuildSettings.connectProfiler = a_settings.m_autoConnectProfiler.value;
            if (a_settings.m_scriptDebugging.enabled)
            {
                EditorUserBuildSettings.allowDebugging = a_settings.m_scriptDebugging.value;
                if (a_settings.m_waitForManagedDebugger.enabled)
                    EditorUserBuildSettings.waitForManagedDebugger = a_settings.m_waitForManagedDebugger.value;
                //UnityEditor.BuildPlayerWindow.DefaultBuildMethods.
                
            }
            if (a_settings.m_scriptsOnly.enabled) EditorUserBuildSettings.buildScriptsOnly = a_settings.m_scriptsOnly;
            AssetDatabase.SaveAssets();
        }


        public BuildOptions GetBuildSettings()
        {
            BuildOptions toReturn = (BuildOptions)0;
            //Development mode
            if(m_settings.m_developmentMode.enabled && m_settings.m_developmentMode.value)
            {
                toReturn = toReturn.SetFlag(BuildOptions.Development);
            
            }
            //Script debugging
            if(m_settings.m_scriptDebugging.enabled && m_settings.m_scriptDebugging.value)
            {
                toReturn = toReturn.SetFlag(BuildOptions.AllowDebugging);
            }
            

            //Script Only
            if (m_settings.m_scriptsOnly.enabled && m_settings.m_scriptsOnly.value)
            {
                toReturn = toReturn.SetFlag(BuildOptions.BuildScriptsOnly);
            }

            //Extras about the resultant build
            if (m_runAfterBuild)
            {
                toReturn= toReturn.SetFlag(BuildOptions.AutoRunPlayer);
            }
            else
            {
                toReturn = toReturn.SetFlag(BuildOptions.ShowBuiltPlayer);
            }

            //if (m_canUpdatePreviousBuild)
            //{
            //    toReturn = toReturn.SetFlag(BuildOptions.AcceptExternalModificationsToPlayer);
            //}
            return toReturn;
        }

        public void RestorePlayerSettings()
        {
            if (m_restorePlayerSettingsAfterBuild) 
            {
                ApplyPlayerSettings(m_playerSettingsBackup);
                //if (EditorUserBuildSettings.activeBuildTarget != m_previousBuildTarget)
                //    EditorUserBuildSettings.SwitchActiveBuildTarget(m_previousBuildTargetGroup, m_previousBuildTarget);
            }

        }

        public BuildPlayerOptions GetBuildOptions()
        {
            BuildPlayerOptions playerOptions = new BuildPlayerOptions();
            //The build targets
            playerOptions.target = m_buildTarget;
            playerOptions.targetGroup = m_buildTargetGroup;
            
            //Sets them to the player settings
            ApplyPlayerSettings();
            BuildAssetsBundle();

            //The path to build the game, relative to the project
            if(m_pathBuild.Length ==0)
            {
                playerOptions.locationPathName = $"Builds/{playerOptions.targetGroup}/{PlayerSettings.productName}";
                Debug.Log("Default path selected: " + playerOptions.locationPathName);
            }
            else
            {
                string localPath = m_pathBuild.ConvertFullPathToProjectPath().AddSlash();
                Debug.Log("Local path selected: " + localPath);
                playerOptions.locationPathName = $"{localPath}{PlayerSettings.productName}";
            }

            //Settings depending on the specified target
            
//#if UNITY_PS4
//                EditorUserBuildSettings.ps4BuildSubtarget = PS4BuildSubtarget.PCHosted;
//                string defaultDirectorySDK = System.Environment.GetEnvironmentVariable("SCE_ROOT_DIR")+"/ORBIS SDKs/";
//                //TODO_KMS Set a variable to define the sdk OR an algorithm to find out the correct SDK to use, and also check if it exists
//                PlayerSettings.PS4.SdkOverride = defaultDirectorySDK + "7.500";
//                Debug.Log("PS4!");
//#elif UNITY_STANDALONE
//                //PS4 creates a subfolder and manages the extension, but Standalone doesn't
//                playerOptions.locationPathName += ".exe";
//                Debug.Log("Standalone!");
//#else
//                Debug.Log("Something else!");
//                //KilianHelper.TriggerFail("PS4 was selected as the target but the directive was not set!");
//#endif
            if(m_buildTarget == BuildTarget.StandaloneWindows)
            {
                playerOptions.locationPathName += ".exe";
            }
            else if(m_buildTarget == BuildTarget.PS4)
            {
                EditorUserBuildSettings.ps4BuildSubtarget = PS4BuildSubtarget.PCHosted;
                string defaultDirectorySDK = System.Environment.GetEnvironmentVariable("SCE_ROOT_DIR") + "/ORBIS SDKs/";
                //TODO_KMS Set a variable to define the sdk OR an algorithm to find out the correct SDK to use, and also check if it exists
                PlayerSettings.PS4.SdkOverride = defaultDirectorySDK + "7.500";
                Debug.Log("PS4!");

            }

            //Scenes and the build options are set on the preset too
            playerOptions.scenes = m_settings.m_scenes.GetPresetScenesPaths();
            playerOptions.options = GetBuildSettings();
            
            return playerOptions;
        }

        //[Button]
        //private void TestBuildSettings()
        //{
           
        //    //m_settings.SetCurrentToPresetScenes();
        //}


#endregion
    }


}
